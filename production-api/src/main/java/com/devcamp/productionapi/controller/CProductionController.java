package com.devcamp.productionapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.productionapi.model.CProduction;
import com.devcamp.productionapi.repository.CProductionRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CProductionController {
    @Autowired
    CProductionRepository cProductionRepository;

    @GetMapping("/productions")

    public ResponseEntity<List<CProduction>> getAllProduction() {
        try {
            List<CProduction> productions = new ArrayList<CProduction>();
            cProductionRepository.findAll().forEach(productions::add);
            return new ResponseEntity<>(productions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
