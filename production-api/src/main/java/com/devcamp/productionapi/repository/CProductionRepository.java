package com.devcamp.productionapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.productionapi.model.CProduction;

public interface CProductionRepository extends JpaRepository<CProduction, Integer> {

}
